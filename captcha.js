var express = require('express');
var cluster = require('cluster');
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var appConfig = require('./core/config/appConfig');
var logger = require('./core/utils/loggerUtil');
var configMaker = require("./core/config/zk/configMaker.js");

if (cluster.isMaster)
{
	var numCPUs = require('os').cpus().length;
    // numCPUs = numCPUs > 4 ? 4 : numCPUs;
    if(process.env.NODE_ENV === "development") numCPUs = 1;
	for (var i = 0; i < numCPUs; i++)
	{
		cluster.fork();
	}
	cluster.on('death', function (worker)
	{
		//Cluster worker dieded. :(
		logger.error(
		{
			error: 'Worker DIED!',
			worker: worker
		}, 'Cluster Worker DIED due to some error!');
		cluster.fork();
	});
	cluster.on('disconnect', function (worker)
	{
		logger.error(
		{
			error: 'Worker DISCONNECTED!',
			worker: worker
		}, 'Cluster Worker DISCONNECTED due to some error!');
		cluster.fork();
	});
}
else
{
	configMaker.init(function()
	{
		var AppConfig = require("./core/config/appConfig");

    if (AppConfig.dnscacheProps && AppConfig.dnscacheProps.enable) {
        require("dnscache")({
            enable: AppConfig.dnscacheProps.enable,
            ttl: AppConfig.dnscacheProps.ttl || 5,
            cachesize: AppConfig.dnscacheProps.cachesize || 1000
        });
    }
		var app = express();
		var router = require('./core/routes/index.js');
		// view engine setup
		app.set('views', path.join(__dirname, 'core/views'));
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded(
		{
			extended: false
		}));
		app.use('/', router);
		app.use(express.static(path.join(__dirname, 'public')));
		// catch 404 and forward to error handler
		app.use(function (req, res, next)
		{
			var err = new Error('Not Found');
			err.status = 404;
			logger.error(err, 'Resource not found.');
			next(err);
		});
		// error handlers
		// development error handler
		// will print stacktrace
		if (app.get('env') === 'development')
		{
			app.use(function (err, req, res, next)
			{
				logger.error(err, 'Internal server error');
				res.status(err.status || 500);
				res.render('error',
				{
					message: err.message,
					error: err
				});
			});
		}
		var port = 5006;
		app.set('port', port);
		//Create HTTP server.
		var server = http.createServer(app);
		//Listen on provided port, on all network interfaces.
		server.listen(port);
		server.on('error', onError);
		server.on('listening', function ()
		{
			console.log("Listening on port " + port)
		});
	});
}
//Normalize a port into a number, string, or false.
function normalizePort(val)
{
	var port = parseInt(val, 10);
	if (isNaN(port))
	{
		// named pipe
		return val;
	}
	if (port >= 0)
	{
		// port number
		return port;
	}
	return false;
}
//Event listener for HTTP server "error" event.
function onError(error)
{
	console.log(error);
	if (error.syscall !== 'listen')
	{
		throw error;
	}
	var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
	// handle specific listen errors with friendly messages
	switch (error.code)
	{
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}
