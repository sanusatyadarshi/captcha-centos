var capImg = document.getElementById('captchaImg'),
    capBtn = document.getElementById('recap-btn'),
    claim = {};

capBtn.addEventListener('click', function() {
    var xhr2 = new XMLHttpRequest();
    claim.userAnswer = document.getElementById('user-answer').value;
    xhr2.addEventListener('load', function() {
        console.log(this.responseText);
    });
    xhr2.open('POST', 'validate');
    xhr2.setRequestHeader('Content-Type', 'application/json');
    xhr2.send(JSON.stringify(claim));
}, false);

var xhr = new XMLHttpRequest();
xhr.addEventListener('load', function() {
    var data = JSON.parse(this.responseText);
/*    claim.token = this.getResponseHeader('token');
    claim.requestId = this.getResponseHeader('requestid');*/
    //console.log(document.cookie);
    capImg.src = data.imageUrl;
});
xhr.open("GET", "/create");
xhr.send();
