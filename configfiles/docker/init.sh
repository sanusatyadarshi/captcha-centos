#!/bin/sh

###################################################################################################################
######   This script is expecting 3 environment variables {SETUP_NAME,ZK_URL,ENV},                         ########
######   and as a part of processes it does following steps                                                ########
######   1. It replaces "docker-stack-name" placeholder with SETUP_NAME in zookeeper.json                  ########
######   2. It executes a process of importing zookeeper properties                                        ########
###################################################################################################################

set -x
python=`which python`


if [ $ENV = 'k8s' ]; then
  # Replace placeholder in zookeeper.json
  sed -i "s/docker-stack-name/$SETUP_NAME/g" /home/deploy/docker/zookeeper_k8s.json
  
  # Start Zookeeper import process
  $python /usr/local/scripts/zk.py --import --overwrite --file /home/deploy/docker/zookeeper_k8s.json $ZK_URL:2181/
else
  # Replace placeholder in zookeeper.json
  sed -i "s/docker-stack-name/$SETUP_NAME/g" /home/deploy/docker/zookeeper.json
  
  # Start Zookeeper import process
  $python /usr/local/scripts/zk.py --import --overwrite --file /home/deploy/docker/zookeeper.json $ZK_URL:2181/
fi

status=$?
if [ $status -ne 0 ]; then
    echo "Failed to run python script : $status"
    exit $status
fi