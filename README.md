# Captcha Service Dependecies #
**We need a Gcc / G++ version >=4.8 to install the canvas npm module which is a core dependency for this application.**

## To install / update G++ or GCC, on Centos: ##
```
sudo yum install centos-release-scl
sudo yum install devtoolset-3-gcc-c++
scl enable devtoolset-3 bash
```


##Post that, we must install canvas npm dependencies.##
### OS X ###
```
brew install pkg-config cairo libpng jpeg giflib
```

### Ubuntu ###
```
sudo apt-get install libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++
```

### Fedora ###
```
sudo yum install cairo cairo-devel cairomm-devel libjpeg-turbo-devel pango pango-devel pangomm pangomm-devel giflib-devel
```

### Solaris ###
```
pkgin install cairo pkg-config xproto renderproto kbproto xextproto
```

## Post installation of the above, please install the following font dependencies: ##
### linux ###

```
sudo yum search arial
sudo yum install liberation-sans-fonts.noarch
```