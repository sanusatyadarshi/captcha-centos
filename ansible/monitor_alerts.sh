#!/bin/bash

OWN_IP=$(hostname -i)

tail -F /home/deploy/captcha/logs/error.log | grep --line-buffered -e "Worker DISCONNECTED" -e "Worker EXITED" -e "EADDRINFO" -e "ECONNABORTED" -e "ECONNREFUSED" -e "ECONNRESET" -e "ENETDOWN" -e "ENETRESET" -e "ENETUNREACH" -e "ETIMEDOUT" | while read LINE ; do (LINE=${LINE//\"/\\\"}; LINE=${LINE//\'/\'\\\\\'\'}; curl -H "Content-Type: application/json" -X POST -d '[{"labels":{"alertname":"captcha Alerts","ip":"'"$OWN_IP"'","env":"Production","logfile":"/home/deploy/captcha/logs/error.log","error":"'"$LINE"'"}}]' http://alertmanager.pdc.games24x7.com/api/v1/alerts); done &