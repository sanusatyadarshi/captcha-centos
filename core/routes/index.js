var express = require('express'),
    router = express.Router();

var createHelper = require('../helpers/createHelper.js'),
    validateHelper = require('../helpers/validateHelper.js');

router.get('/create', createHelper.create);

router.post('/validate', validateHelper.validate);

module.exports = router;
