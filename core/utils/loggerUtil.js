var bunyan = require('bunyan');
var util = bunyan.createLogger({
    name: 'captcha',
    serializers: bunyan.stdSerializers,
	streams:
	[
		{
			stream: process.stdout,
			level: 'info'
		},
		{
			stream: process.stderr,
			level: 'error',
		}
	]
});

module.exports = util;
