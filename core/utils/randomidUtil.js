var randomid = {};

randomid.generate = function(idLength) {
    var id = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    idLength = idLength || 5;
    for (var i = 0; i < idLength; i++)
        id += possible.charAt(Math.floor(Math.random() * possible.length));

    return id;
}

module.exports = randomid;