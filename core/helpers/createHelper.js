var imageService = require('../services/imageService');
var redisService = require('../services/redisService');
var randomid = require('../utils/randomidUtil');
var logger = require('../utils/loggerUtil');
var appConfig = require('../config/appConfig');
var uuid = require('uuid');
var helper = {};
helper.create = function (req, res, next)
{
    logger.info('Received : create request');
    var captcha = imageService.create();
    var uuidToken = "captcha_"+uuid.v4();
    redisService.setData(uuidToken, captcha.answer, function (err)
    {
        // Redis set callback.
        if (err)
        {
            return next(err);
        }
        res.cookie(appConfig.cookiename, {'token': uuidToken });
        res.send(
        {
            imageUrl: captcha.url,
            token: uuidToken
        });
        res.end();
        logger.info('Create request response sent.');
    });
    return;
};
module.exports = helper;