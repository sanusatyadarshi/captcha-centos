var redisService = require('../services/redisService');
var appConfig = require('../config/appConfig');
var logger = require('../utils/loggerUtil');
var cookie = require('cookie');
var helper = {};
helper.validate = function (req, res, next)
{
	var key = req.headers[appConfig.cookiename];
	var data = req.body;
	
	//Check if the client is sending the data in which case the cookie will be in headers and not as a header
	if(!key)
	{
        if(req.headers.cookie && typeof req.headers.cookie === "string")
        {
            req.mycookies = cookie.parse(req.headers.cookie);
        }
		key = req.mycookies && req.mycookies[appConfig.cookiename];
	}

	if(!key)
	{
		res.status(200).json({valid: false});
		return;
	}

	redisService.getData(key, function (err, value)
	{
		// Verify empty keys returned by redis cluster.
		if (err)
		{
			return next(err);
		}
		
		if(data.userAnswer && data.userAnswer === value)
		{
			res.status(200).json({valid: true});
		}
		else
		{
			res.status(200).json({valid: false});
		}
	});
};
module.exports = helper;

