var appConfig = require('./appConfig.js');

var zkRoot = process.env.ZK_ROOT
if(!zkRoot) zkRoot = "FE"

var config = {
    //ZK sever config
    /**
     * Put in your config overrides here.
     * Precedence : UUID (heighest precedence) > local > global 
     **/

     // Allowed type for global configuration is 
     // int, float, string, object

    //Global properties.
    global: [
    {
        path: `/${zkRoot}/global/commonRedisClusterNodes`,
        target: appConfig,
        propertyName: 'redisClusterNodes',
        type: "object"
    }],
    
    //Local properties.
    basePath: `/${zkRoot}/captcha-service`, //Base path required for fetching ZK nodes
    
    local: [
    {
        path: '/appconfig',
        target: appConfig
    }]
}

module.exports = config;