var events = require('events');

var appConfig = {};

appConfig.emitter = new events.EventEmitter();
appConfig.updateCallback = function (strPropertyName) 
{
    console.log("+++++++++++++++appConfig updateCallback++++++++++++++++++++");
    console.log(strPropertyName);
    console.log(appConfig[strPropertyName]);
    console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

    
    appConfig.emitter.emit(strPropertyName + "Changed");
};

module.exports = appConfig;