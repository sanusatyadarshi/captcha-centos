var fs = require('fs');
var path = require('path');
var async = require('async');
var getmac = require('./getmac');
var _ = require('lodash');

var logger = require('../../utils/loggerUtil.js');
var zkClient = require('./zkClient');
var zkConfig = require('../zkConfig.js');
try
{
    var zkConnectionString = require('./connectionString.js');
}
catch(e)
{
    
}

var maker = {
    init: function(appCallback) {
        var self = this;
        if(!zkConnectionString) zkConnectionString = self.getZKserver();
        zkClient.init(zkConnectionString, function() {
            async.series([
                self._fetchGlobalProps.bind(self),
                self._fetchLocalProps.bind(self),
                self._fetchUUIDProps.bind(self)
            ], function() {
                logger.info({}, 'Everything has been fetched ......');
                appCallback();
            });
        });
    },
    _fetchGlobalProps: function(asyncCallback) {
        var self = this;
        var tasks = [];
        if (zkConfig.global.length > 0) {
            zkConfig.global.forEach(function(config, i) {
                config.cache = {};

                var iterateeFunc = function(data, updateCallback) 
                {
                    if(data)
                    {
                        value = GetValueFromString(config.type, data);
                        if(value)
                        {
                            config.cache[config.propertyName] = value;
                        }
                    }
                    else
                    {
                        delete config.cache[config.propertyName];
                    }
                    self.resolve(config.target, config.propertyName, updateCallback);
                };

                var taskFunc = function(tasksCallback) {
                    
                    zkClient.fetchProperty(
                        config.path,
                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker Error.');
                            } else {
                                iterateeFunc(data);
                            }
                            tasksCallback(null);
                        },

                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker UpdateCallback Error.');
                            } else {
                                iterateeFunc(data, config.target.updateCallback);
                            }
                        }
                    );
                }
                tasks.push(taskFunc);
            })

            async.series(tasks, function() {
                logger.info({}, 'All global properties fetched.');
                asyncCallback(null);
            });
        }
    },
    _fetchLocalProps: function(asyncCallback) {
        var tasks = [];
        var self = this;
        if (zkConfig.local.length > 0) {

            zkConfig.local.forEach(function(config, i) {
                config.cache = {};

                var iterateeFunc = function(data, updateCallback) 
                {
                    var arrKeys = Object.keys(config.cache);
                    if(data)
                    {
                        value = GetValueFromString('object', data);
                        if(value)
                        {
                            for(key in value)
                            {
                                var bPresent = false;
                                for (var j = 0; j < arrKeys.length; j++) 
                                {
                                    if(arrKeys[j] == key)
                                    {
                                        bPresent = true;
                                        break;
                                    }
                                }
                                if(!bPresent)
                                    arrKeys[arrKeys.length] = key;
                            }
                            config.cache = value;
                        }
                    }
                    else
                    {
                        config.cache = {};
                    }
                    for (var j = 0; j < arrKeys.length; j++) 
                    {
                        self.resolve(config.target, arrKeys[j], updateCallback);
                    }
                };
                var taskFunc = function(tasksCallback) {
                    zkClient.fetchProperty(
                        zkConfig.basePath + config.path,
                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker Error.');
                            } else {
                                iterateeFunc(data);
                            }
                            tasksCallback(null);
                        },
                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker UpdateCallback Error.');
                            } else {
                                iterateeFunc(data, config.target.updateCallback);
                            }
                        });
                };

                tasks.push(taskFunc);
            });

            async.series(tasks, function() {
                logger.info({}, 'All Local properties fetched');
                asyncCallback(null);
            });
        }
    },
    _fetchUUIDProps: function(asyncCallback) {
        var tasks = [];
        var self = this;
        var UUID_path = zkConfig.basePath + '/' + self.getUUID();
        if (zkConfig.local.length > 0) {
            zkConfig.local.forEach(function(config, i) {                
                config.cache_uuid = {};

                var iterateeFunc = function(data, updateCallback) 
                {
                    var arrKeys = Object.keys(config.cache_uuid);
                    if(data)
                    {
                        value = GetValueFromString('object', data);
                        if(value)
                        {
                            for(key in value)
                            {
                                var bPresent = false;
                                for (var j = 0; j < arrKeys.length; j++) 
                                {
                                    if(arrKeys[j] == key)
                                    {
                                        bPresent = true;
                                        break;
                                    }
                                }
                                if(!bPresent)
                                    arrKeys[arrKeys.length] = key;
                            }
                            config.cache_uuid = value;
                        }
                    }
                    else
                    {
                        config.cache_uuid = {};
                    }
                    for (var j = 0; j < arrKeys.length; j++) 
                    {
                        self.resolve(config.target, arrKeys[j], updateCallback);
                    }
                };
                var taskFunc = function(tasksCallback) {
                    zkClient.fetchProperty(
                        UUID_path + config.path,
                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker Error.');
                            } else {
                                iterateeFunc(data);
                            }
                            tasksCallback(null);
                        },
                        function(err, data) {
                            if (err) {
                                logger.error(err, 'Config Maker UpdateCallback Error.');
                            } else {
                                iterateeFunc(data, config.target.updateCallback);
                            }
                        });
                };

                tasks.push(taskFunc);
            });

            async.series(tasks, function() {
                logger.info({}, 'All UUID properties fetched');
                asyncCallback(null);
            });
        }
    },
    resolve: function(target, propertyName, updateCallbackCallback) {
        var oldPropertyValue = target[propertyName];

        updateCallbackCallback = updateCallbackCallback || function() {};
        if (zkConfig.global.length > 0)
        {
            zkConfig.global.forEach(function (config)
            {
                if (config.target == target && 
                    config.cache && config.cache.hasOwnProperty(propertyName))
                {
                    target[propertyName] = config.cache[propertyName];
                }
            });
        }

        if (zkConfig.local.length > 0)
        {
            zkConfig.local.forEach(function (config)
            {
                if (config.target == target && 
                    config.cache && config.cache.hasOwnProperty(propertyName))
                {
                    target[propertyName] = config.cache[propertyName];
                }
            });
        }

        if (zkConfig.local.length > 0) {
            zkConfig.local.forEach(function(config) {
               if (config.target == target && 
                    config.cache_uuid && config.cache_uuid.hasOwnProperty(propertyName))
                {
                    target[propertyName] = config.cache_uuid[propertyName];
                }
            });
        }

        if (!_.isEqual(oldPropertyValue, target[propertyName])) 
        {
            updateCallbackCallback(propertyName);
        }
    },
    getUUID: function() {
        return getmac.GetMacAddress();
    },
    getZKserver: function() {
        var connections = {
            development: "zk1-stagepf.rummycircle.com:2181,zk2-stagepf.rummycircle.com:2181,zk3-stagepf.rummycircle.com:2181",
            production: "zookeeper1.pdc.games24x7.com:2182,zookeeper2.pdc.games24x7.com:2183,zookeeper3.pdc.games24x7.com:2182"
        };

        if(process.env.ZK_URL) {
            return process.env.ZK_URL
        }

        return connections[process.env.NODE_ENV] || connections.production
    }
};

function GetValueFromString(type, data)
{
    var value = null;
    if(data)
    {
        try
        {
            if(type == "int")
            {
                value = parseInt(data);
                if(isNaN(data))
                {
                    value = null;
                }
            }
            else if(type == "float")
            {
                value = parseFloat(data);
                if(isNaN(data))
                {
                    value = null;
                }
            }
            else if(type == "string")
            {
                value = data;
            }
            else if(type == "object")
            {
                value = JSON.parse(data);
            }
        }
        catch(ex)
        {
            logger.error(ex, 'Failed to parse property from zookeeper: ' + data)
        }
    }
    return value;
}

module.exports = maker;
