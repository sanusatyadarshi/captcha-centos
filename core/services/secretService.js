/*
    Module: Retrieves/Generates secret based on "cache" flag in appconfig
*/
var fs = require('fs');

var appConfig = require('../config/appConfig'),
    randomid = require('../utils/randomidUtil');


var service = {};

// Retrieves secret from appconfig or generate one itself.
service.retrieve = function(secretLength) {

    secretLength = secretLength || 5;

    if (appConfig.cache) {
        return randomid.generate(secretLength);
    } else {
        //If "secret" has not been generated at appconfig level, generate one for the Master.
        return fs.readFileSync('private_key.txt', 'utf-8');
    }

};

// Sets a single copy of private secret for redis connection fallback.
// Should be called at the start.
service.setPrivateKey = function(secretLength) {
    var secret = randomid.generate(secretLength);
    fs.writeFileSync('private_key.txt', secret, 'utf-8');
}

module.exports = service;
