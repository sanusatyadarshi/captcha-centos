const { createCanvas } = require('canvas')

var randomid = require('../utils/randomidUtil'),
    appConfig = require('../config/appConfig');

var service = {};

service.create = function() {
    var randomText = randomid.generate(appConfig.captchaLength);
    var newCanvas = createCanvas(150, 40);
    var ctx = newCanvas.getContext('2d');
    ctx.font = '30px Impact';
   
    ctx.fillText(randomText, 25, 30);

    // To create line under text.
    var te = ctx.measureText(randomText);
    ctx.strokeStyle = 'rgba(0,0,0,0.5)';
    ctx.beginPath();
    ctx.lineTo(50, 102);
    ctx.lineTo(25 + te.width, 102);
    ctx.stroke();
    return {
        url: newCanvas.toDataURL(),
        answer: randomText
    };
}

module.exports = service;
