/**
	Module: Redis cache service.
*/
var Redis = require('ioredis');
var appConfig = require('../config/appConfig');
var logger = require('../utils/loggerUtil');
var service = {};
var cluster = new Redis.Cluster(appConfig.redisClusterNodes.hosts,
{
	showFriendlyErrorStack: true
});
cluster.on('connect', function ()
{
	logger.info('REDIS: Cluster connected.');
});
cluster.on('close', function ()
{
	logger.info('REDIS: Disconnected from the server.');
});
cluster.on('reconnecting', function ()
{
	logger.info('REDIS: Trying to connect to a redis server.');
});
cluster.on('error', function (err)
{
	logger.error(err, 'REDIS: Connection failed due an Error.');
});
cluster.on('end', function ()
{
	logger.info('REDIS: Connection was ended from server.');
});
service.setData = function (key, value, callback)
{
	//Set the data only for 5 minutes
	cluster.setex(key, appConfig.tokenexpiry, value, function (err, result)
	{
		if (err)
		{
			logger.error(err, 'REDIS: Error in setting the provided key into server.');
			return callback(err);
		}
		logger.info(key, 'REDIS: Key set in the server.');
		callback(null);
	});
};
service.getData = function (key, callback)
{
	cluster.get(key, function (err, result)
	{
		if (err)
		{
			logger.error(err, 'REDIS: Some error in retreiving key from server.');
			return callback(err); // Return err if key was not available or not retrieved.
		}
		logger.info(result, 'REDIS: Key retrieved from the server.');
		
		//We need to delete the key as well
		cluster.del(key, function(err, res)
		{
			//Key was deleted
			if(err)
			{
				logger.error(err, 'REDIS: Some error in retreiving key from server.');
			}
			callback(null, result);
		});
	});
};
module.exports = service;

