#Build stage
FROM mhart/alpine-node:8.16.0 as build



RUN mkdir -p /home/deploy/captcha/node_modules/
RUN mkdir -p /home/deploy/captcha/logs/
ADD package.json /home/deploy/captcha/package.json

RUN apk add --no-cache \
    build-base \
    g++ \
    cairo-dev \
    jpeg-dev \
    pango-dev \
    bash \
    imagemagick \
    && cd /home/deploy/captcha/ && npm install --only=production

#Deploy stage
FROM mhart/alpine-node:slim-8.16.0

RUN mkdir -p /home/deploy/captcha/node_modules/ \
    && mkdir -p /home/deploy/captcha/logs/ \
    && apk update \
    && apk add --no-cache \
    wget \
    cairo-dev \
    jpeg-dev \
    pango-dev \
    && apk add --update --repository http://dl-3.alpinelinux.org/alpine/edge/releases libmount ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family fontconfig

COPY --from=build /home/deploy/captcha/node_modules /home/deploy/captcha/node_modules

WORKDIR /home/deploy/captcha
ADD captcha.js /home/deploy/captcha/captcha.js
ADD core /home/deploy/captcha/core/

#zk segregation pre requisites
RUN apk add --no-cache python && python -m ensurepip && pip install kazoo
COPY configfiles/docker/zookeeper.json /home/deploy/docker/zookeeper.json
COPY configfiles/docker/zookeeper_k8s.json /home/deploy/docker/zookeeper_k8s.json
COPY configfiles/docker/init.sh /usr/local/scripts/init.sh
COPY configfiles/docker/startup.sh /home/deploy/docker/startup.sh
RUN chmod +x /usr/local/scripts/init.sh
RUN chmod +x /home/deploy/docker/startup.sh

EXPOSE 5006

ARG ZK_URL
ARG ZK_ROOT=FE
ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV}
ENV ZK_ROOT=${ZK_ROOT}
ENV ZK_URL=${ZK_URL}

ENTRYPOINT ["/bin/sh","/home/deploy/docker/startup.sh"]