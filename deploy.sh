#!/bin/sh

BUILD_FILES='assets/*'
DEPLOY_DEST='/home/deploy/captcha/'
source /home/tomcat/.nvm/nvm.sh

tar -xzf captcha.tar
echo "===============TAR SUCCESSFULLY UNZIPPED==================="

mkdir -p $DEPLOY_DEST
rsync -ar $BUILD_FILES $DEPLOY_DEST --exclude="*configMaker*" --exclude="*zkConfig*"

echo "===============SUCCESSFULLY COPIED RESOURCES==================="

cd $DEPLOY_DEST
echo "===============INSTALLING NPM DEPENDENCIES==================="
npm install
echo "===============NPM DEPENDENCIES DONE==================="

echo "===============STARTING PM2 APP==================="
pm2 startOrRestart startCaptcha.json